use iced::widget::{button, column, text,text_input};
use iced::{Alignment, Element, Sandbox, Settings};

pub fn main() -> iced::Result {
    Counter::run(Settings::default())
}

struct Counter {
    value: i32,
    field:String,
}

#[derive(Debug, Clone)]
enum Message {
    IncrementPressed,
    DecrementPressed,
    TextChanged(String),
    Submit
}

impl Sandbox for Counter {
    type Message = Message;

    fn new() -> Self {
        Self { value: 0,field:String::new() }
    }

    fn title(&self) -> String {
        String::from("Counter - Iced")
    }

    fn update(&mut self, message: Message) {
        match message {
            Message::IncrementPressed => {
                self.value += 1;
            }
            Message::DecrementPressed => {
                self.value -= 1;
            }
            Message::TextChanged(txt) => {
                self.field = txt;
                }
            Message::Submit => {
                if let Ok(tmp) = self.field.parse::<i32>(){
                    self.value = tmp;
                }
            }
        }
    }

    fn view(&self) -> Element<Message> {
        column![
            button("Increment").on_press(Message::IncrementPressed),
            text(self.value).size(50),
            button("Decrement").on_press(Message::DecrementPressed),
            text_input("number input",&self.field ,Message::TextChanged).on_submit(Message::Submit),
            
        ]
        .padding(20)
        .align_items(Alignment::Center)
        .into()
    }
}
