#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release

use eframe::egui;

fn main() -> Result<(), eframe::Error> {

    let options = eframe::NativeOptions {
        initial_window_size: Some(egui::vec2(200.0, 300.0)),
        ..Default::default()
    };
    eframe::run_native(
        "My egui App",
        options,
        Box::new(|_cc| Box::<MyApp>::default()),
    )
}

struct MyApp {
    valor: i32,
    entry: String
}

impl Default for MyApp {
    fn default() -> Self {
        Self {
            valor: 42,
            entry:String::new()
        }
    }
}

impl eframe::App for MyApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.heading("Counter Application");

            ui.label(format!("{}", self.valor));
            if ui.button("increment").clicked() {
                self.valor += 1;
            }
            if ui.button("decrement").clicked(){
                self.valor -= 1;
            }
             if ui.text_edit_singleline(&mut self.entry).lost_focus(){
                if let Ok(number) = self.entry.parse::<i32>(){
                    self.valor = number;
                };

            }
        });
    }
}
