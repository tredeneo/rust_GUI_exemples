slint::include_modules!();
fn main() {
    let ui = AppWindow::new().unwrap();

    let increment = ui.as_weak();
    let decrement = ui.as_weak();
    let submit = ui.as_weak();
    ui.on_increment(move || {
        let handle = increment.unwrap();
        
        handle.set_counter(handle.get_counter() + 1);
    });
    ui.on_decrement(move || {
        let ui = decrement.unwrap();
        ui.set_counter(ui.get_counter()-1);
    });
    ui.on_submit(move || {
        let ui = submit.unwrap();
        if let Ok(number) = ui.get_field().parse::<i32>() {
            ui.set_counter(number);
        };
    });
    ui.run().unwrap();
}
