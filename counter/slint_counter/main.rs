slint::include_modules!();
fn main() {
    let ui = AppWindow::new().unwrap();


    let ui_handle = ui.as_weak();
    ui.on_increment(move || {
        let ui = ui_handle.unwrap();
        ui.set_counter(ui.get_counter() + 1);
    });
    ui.on_decrement(move || {
        let ui = ui_handle.unwrap();
        ui.set_counter(ui.get_counter()-1);
    });
    ui.run();
}
